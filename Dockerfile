FROM eclipse-temurin:17-jdk-alpine
ARG JAR_FILE
COPY ${JAR_FILE} demo-prod.jar
ENTRYPOINT ["java","-jar","/demo-prod.jar"]
