package com.swisscom.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MigrationprojectforgitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(MigrationprojectforgitlabApplication.class, args);
	}

}
